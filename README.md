# COD3R - API com GraphQL

This code is part of the [API com GraphQL](https://www.youtube.com/watch?v=0WWuJaNSFgk&list=PLdPPE0hUkt0okslA6u6hXw_2wQ6WM4d2-) video series by COD3R.

## Get Started
1. Install [Node](https://nodejs.org).
2. Install [MySQL](https://dev.mysql.com/downloads/workbench/).
3. Create a new database schema called graphql.
4. Clone the git repository.
5. Access the directory where the repository was created.
6. Install dependencies: `npm i`.
7. Install knex dependency globally: `npm i -g knex`.
8. Create table users running the command: `knex migrate:make create_table_users`.
9. Start application: `node server.js`.
10. Access the URL: `http://localhost:4000/graphql`.

## Other Commands
* `knex init`: Create/access directory src and run this command to generate knexfile.js.
* `knex migrate:make create_table_users`: Create table users.
* `knex migrate:latest`: Run migrations. 
* `node config/database.js`: Run database DML commands.

## Queries and Mutations
Get user information: 
```
query GetUsers {
  getUsers {
    name
    email
  }
  getUser(id: 2) {
    name
    email
  }
}
```
Create new user: 
```
mutation CreateUser {
  createUser(input: {
    name: "Gui"
    email: "gui@test.com"
    password: "123456"
  }) {
    id
    name
    email
  }
}
```